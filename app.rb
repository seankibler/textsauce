require 'sinatra'
require 'twilio-ruby'
require 'json'

error 403 do
  'You can not be here!'
end

get '/' do
  redirect to('/endpoints')
end

get '/endpoints' do
  content_type :json

  {endpoints: [
    'sms' => {
      'endpoint' => url('/receivesms'),
      'description' => 'Twilio compatible web hook for receiving text messages.',
      'method' => 'POST'
    }
  ]}.to_json
end

post '/sms/receive' do
  if params['AccountSid'] != ENV['TWILIO_ACCOUNT_SID']
    status 403
    return
  end

  action = params['Body'].split(/\W/).first.to_s.downcase.to_sym

  message_content = case action
                    when :directions
                      destination = params['Body'].split(/\W/).drop(1).join(' ')
                      "Good luck, happy travels #{destination}!"
                    else
                      "No such action #{action} bub! Supported actions: directions."
                    end

  twiml = Twilio::TwiML::Response.new do |r|
    r.Message message_content,
      to: params['From'],
      from: params['To']
  end

  twiml.text
end
